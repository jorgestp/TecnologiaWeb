<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	 	position: absolute;
  		bottom: 0;
  		margin:auto
}

#tablaExito{

	background:#00C;
	padding:10px;
	border:solid 2px #FF0000;
	text-align: center;
	margin: 0 auto;

}

</style>

</head>
<body>
			
	
         	<header>
      			<h1>Este ser�a el titulo de nuestra web</h1>
        	</header>
        	
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
            	<ul>
            	    <c:url var="vueltaInicioAdmin" value="ManagerCine">
			 		<c:param name="instruccion" value="vueltaInicioAdmin"></c:param>	
					</c:url>
					<li><a href="${vueltaInicioAdmin}">Inicio</a></li>
                	<li><a href="#">Promociones</a></li>
                	<li><a href="#">Salir</a></li>
					<li>${sessionScope.usuario}</li>
					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
			<section>
			
			
            
             <aside>
             
             	<c:url var="pelis" value="ManagerCine">
			 	<c:param name="instruccion" value="formalizarInserccionPelicula"></c:param>	
				</c:url>
        	    <c:url var="salas" value="ManagerCine">
			 	<c:param name="instruccion" value="salas"></c:param>	
				</c:url>
        	    <c:url var="precio" value="ManagerCine">
			 	<c:param name="instruccion" value="precio"></c:param>	
				</c:url>
            <blockquote><a href="${pelis}">A�adir Peliculas<a></a></blockquote>
            <blockquote><a href="${salas}">Configurar Salas</a></blockquote>
            <blockquote><a href="${precio}">Configurar Precio</a></blockquote>
        	
       		 </aside>
             
             <section>
             
        <table width="276" align="center" cellpadding="1">
    		<tr>
      			<td width="13%"><label for="id">ID:</label></td>
      			<td width="87%"><label id="id">${pelicula.id}</label></td>
    		</tr>
    		<tr>
     			<td><label for="nombre">Nombre:</label></td>
      			<td><label id="nombre">${pelicula.nombre}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Publicacion:</label></td>
      			<td><label id="publicacion">${pelicula.a�opublicacion}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Puntuacion:</label></td>
      			<td><label id="publicacion">${pelicula.puntos}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">SALA ACTUAL:</label></td>
      			<td><label id="publicacion">${pelicula.sala}</label></td>
    		</tr>
    		<tr>  
  		</table>           

  <form action="ManagerCine" method="get">
  
  	<input type="hidden" name="instruccion" value="formalizarSala">
  	<input type="hidden" name="id" value="${pelicula.id}">     	
    <p>Elige Sala;</p> 
    <label>
      <input type="radio" name="articulos" value="1" disabled="disabled">
      1</label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="2" disabled="disabled">
    2 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="3" disabled="disabled">
      3 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="4" disabled="disabled">
    4 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="5" disabled="disabled">
    5 </label>
      &nbsp;
    <label>
      <input type="radio" name="articulos" value="6" disabled="disabled">
    6 </label>

  
  	<br>
    <br>
    <input type="submit" name="button" id="button" value="Enviar" disabled="disabled">
    </p>
    
    </form>

    </form>
    <table id="tablaExito" width="200" border="1">
      <tr>
        <td> <h3 style="color: #0F0;"> GUARDADA SALA CORRECTAMENTE!</h3></td>
      </tr>
    </table>
    <br>
</section>

<footer><small>Derechos Reservados</small> <address>666666666</address></footer>

</body>
</html>