<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

section{
	background-color: #FF6;
	margin: auto;
	text-align: center;
	
}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	 	position: absolute;
  		bottom: 0;
  		margin:auto
}

</style>

</head>
<body>
	
         	<header>
      			<h1>Practica Tecnologias Web 17/18</h1>
        	</header>
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
            	<ul>
            		<li><a href="quienessomosVistaAdministrador.jsp">Quienes somos</a></li>
                	<li><a href="#">Promociones</a></li>
                	<li><a href="#">Salir</a></li>
					<li>${sessionScope.usuario}</li>
					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
			<section>
			
			
            
             <aside>
             
             	<c:url var="pelis" value="ManagerCine">
			 	<c:param name="instruccion" value="formalizarInserccionPelicula"></c:param>	
				</c:url>
        	    <c:url var="salas" value="ManagerCine">
			 	<c:param name="instruccion" value="salas"></c:param>	
				</c:url>
        	    <c:url var="precio" value="ManagerCine">
			 	<c:param name="instruccion" value="precio"></c:param>	
				</c:url>
            <blockquote><a href="${pelis}">Aņadir Peliculas<a></a></blockquote>
            <blockquote><a href="${salas}">Configurar Salas</a></blockquote>
            <blockquote><a href="${precio}">Configurar Precio</a></blockquote>
            
        	
       		 </aside>
			
				<table>
		
					<tr>	<!-- Representacion de la primera fila de una tabla con la etiqueta <tr> -->
						<td class="cabecera">ID</td>
						<td class="cabecera">NOMBRE</td>
						<td class="cabecera">PUBLICACION</td>
						<td class="cabecera">PUNTUACION</td>
						<td class="cabecera">SALA</td>
						<td class="cabecera">PRECIO</td>
						

					</tr>
	

				<c:forEach var="temp" items="${LISTAPELI}">
					
					
				
					<tr><!-- Representacion de otra fila-->
					<td class="filas">${temp.id}</td>
					<td class="filas">${temp.nombre}</td>
					<td class="filas">${temp.aņopublicacion}</td>
					<td class="filas">${temp.puntos}</td>
					<td class="filas">${temp.sala}</td>
					<td class="filas">${temp.precio}</td>		
					</tr>
		
				</c:forEach>
	
				</table>

            </section>

        <footer><small>Derechos Reservados</small> <address>666666666</address></footer>

</body>
</html>