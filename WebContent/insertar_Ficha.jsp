<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

section{
	background-color: #FF6;
	margin: auto;
	text-align: center;
}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	position: absolute;
	bottom: -2px;
	margin: auto;
	left: 509px;
}

.boton {
	text-align: center;
}
</style>

</head>
<body>
	
         	<header>
      			<h1>Practica Tecnologias Web 17/18</h1>
        	</header>
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
            	<ul>
            	    <c:url var="vueltaInicioAdmin" value="ManagerCine">
			 		<c:param name="instruccion" value="vueltaInicioAdmin"></c:param>	
					</c:url>
					<li><a href="${vueltaInicioAdmin}">Inicio</a></li>
                	<li><a href="#">Promociones</a></li>
                	<li><a href="#">Salir</a></li>
					<li>${sessionScope.usuario}</li>
					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
<section>
			
			

             <aside>
             
             
             	<c:url var="pelis" value="ManagerCine">
			 	<c:param name="instruccion" value="formalizarInserccionPelicula"></c:param>	
				</c:url>
        	    <c:url var="salas" value="ManagerCine">
			 	<c:param name="instruccion" value="salas"></c:param>	
				</c:url>
        	    <c:url var="precio" value="ManagerCine">
			 	<c:param name="instruccion" value="precio"></c:param>	
				</c:url>
            <blockquote><a href="${pelis}">A�adir Peliculas<a></a></blockquote>
            <blockquote><a href="${salas}">Configurar Salas</a></blockquote>
            <blockquote><a href="${precio}">Configurar Precio</a></blockquote>
            
        	
       		 </aside>
			
	

	<input type="hidden" name="instruccion" value="insertaPelicula">
  
		
	<h1 style="text-align:center">FICHA</h1>
<form  method="get" action="ManagerCine">

  	<input type="hidden" name="instruccion" value="formalizarFicha">
  	<input type="hidden" name="id" value="${pelicula.id}"> 
  	
  	
  <table width="517" height="179" border="0" align="center" cellspacing="1">
    <tr>
      <th width="110" scope="row">NOMBRE</th>
      <td width="346">${pelicula.nombre}</td>
    </tr>
    <tr>
      <th scope="row">DESCRIPCION</th>
      <td><label>
        <textarea name="descripcion" id="descripcion" cols="45" rows="3">Escriba la descripcion de la pelicula</textarea>
      </label></td>
    </tr>
    <tr>
      <th scope="row">CATEGORIA</th>
      <td><label>
        <select name="categoria" size="1" id="categoria">
          <option>Comedia</option>
          <option>Suspense</option>
          <option>Terror</option>
          <option>Animacion</option>
          <option>C.Ficcion</option>
        </select>
      </label></td>
    </tr>
    <tr>
      <th scope="row">ACTOR</th>
      <td><label>
        <input name="actor" type="text" id="actor" value="Nombre del Actor principal" size="30">
      </label></td>
    </tr>
    <tr>
      <th scope="row">DIRECTOR</th>
      <td><label>
        <input name="director" type="text" id="director" value="Nombre del Director">
      </label></td>
    </tr>
    <tr>
    <th height="26"></th>
    <td><label class="boton">
      <input type="submit" name="enviar" id="enviar" value="Enviar">
    </label></td>
    </tr>
  </table>
</form>

            
</section>

<footer><small>Derechos Reservados</small> <address>666666666</address></footer>


</body>
</html>