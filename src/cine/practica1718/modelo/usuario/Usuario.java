package cine.practica1718.modelo.usuario;
/**
 * 
 *  @author 47536486V y 72978510Q
 *
 */
public abstract class Usuario {

	 private String nombre, apellido, user, password;

	public Usuario(String nombre, String apellido, String user, 
			String password) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.user = user;
		this.password = password;
		
	}
	
	/*
	 * METODOS GETER Y SETER
	 */

	public Usuario(String user, String pss) {
		
		this.user = user;
		
		this.password=pss;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	
	
}
