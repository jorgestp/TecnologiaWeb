/**
 * 
 */
package cine.practica1718.modelo.usuario;

/**
 *  @author 47536486V y 72978510Q
 *
 *Representa el usuario administrador de la aplicacion web
 */
public class Administrador extends Usuario {

	public Administrador(String nombre, String apellido,
			String user, String password) {
		
		super(nombre, apellido, user, password);

	}

}
