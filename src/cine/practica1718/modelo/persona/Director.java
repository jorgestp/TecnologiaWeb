package cine.practica1718.modelo.persona;

import java.util.Date;

/**
 * 
 * @author 47536486V y 72978510Q
 *
 *Representa a un Director y por el momento solo da cuerpo a los metodos
 *heredados por la clase abstracta persona
 */

public class Director extends Persona{

	
	public Director(String nombre, String apellido, String nacionalidad, 
			int edad, Date fecha) {
		
		super(nombre, apellido, nacionalidad, edad, fecha);

	}



}
