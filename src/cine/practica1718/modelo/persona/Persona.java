package cine.practica1718.modelo.persona;

import java.util.Date;

/**
 * 
 * @author 47536486V y 72978510Q
 *
 *Clase abstracta 
 */
public abstract class Persona {

	private String nombre, apellido, nacionalidad;
	private int edad;
	private Date fecha;
	
	
	public Persona(String nombre, String apellido, String nacionalidad,
			int edad, Date fecha) {
		
		this.nombre = nombre;
		this.apellido = apellido;
		this.nacionalidad = nacionalidad;
		this.edad = edad;
		this.fecha = fecha;
		
	}
	
	
	
	/*
	 * METODOS GETTERS Y SETTERS DE LOS ATRIBUTOS DE LA CLASE
	 */
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	


}
