package cine.practica1718.modelo.persona;

import java.util.Date;
/**
 * 
 *  @author 47536486V y 72978510Q
 *
 *Clase que representa al Actor y que por el momento solo le da cuerpo 
 *a los atributos heredados de la clase abstracta Persona
 */

public class Actor extends Persona {

	public Actor(String nombre, String apellido, String nacionalidad, 
			int edad, Date fecha) {
		
		super(nombre, apellido, nacionalidad, edad, fecha);
		
	}

}
