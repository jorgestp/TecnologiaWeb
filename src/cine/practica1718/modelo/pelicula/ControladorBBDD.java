package cine.practica1718.modelo.pelicula;

import java.io.IOException;
import java.sql.*;


import java.io.BufferedReader;
import java.io.FileReader;

/**
 * 
 * @author Ruben Com�n y Jorge
 * @version 1.1
 * 
 * Clase encargada de gestionar las solicitudes a la base de datos, crear tablas iniciales y valores por
 * defecto.
 * 
 */
public class ControladorBBDD {
	
	private Connection conexion;
	
	/**
	 * 
	 * @param conexion, es el paremetro con el que realizar� la conexion a la db.
	 * @param path se inicializa con el path relativo al proyecto.
	 * @throws IOException 
	 */
	public ControladorBBDD(Connection conn){
		this.conexion = conn;
	}
	
	/**
	 * Realiza la inicializaci�n de la db en memoria.
	 * @throws IOException
	 */
	public void loadScript(String path) throws IOException{
	
	try {
	
	// Carga el contenido del script, para poder realizar la consulta.
	String cadena =  getContentFile(path);

	Statement query = conexion.createStatement();
			
	query.executeUpdate(cadena);
	
    System.out.format("*Creando script %s....",path);

	query.close();

	}catch (Exception e) {
		System.out.println(e.getMessage());
	}
		
}

public void loadValueDefault() throws IOException{
	
	try {
		
	// Carga el contenido del escript para poder realizar la consulta.
	String cadena = getContentFile(System.getProperty("user.dir") + "\\BBDD\\Value_Default.script");

	Statement query = conexion.createStatement();
	
	query.executeUpdate(cadena);
	
    System.out.println("* Cargando valores por defecto....");

	query.close();

	}catch (Exception e) {
		System.out.println(e.getMessage());
	}
		
}

	/**
	 * Realiza la lectura del fichero, y devuelve el contenido del mismo.
	 * @param path Ubicaci�n donde esta el fichero.
	 * @return contenido del fichero.
	 * @throws IOException
	 */
	private String getContentFile(String path) throws IOException{
		String cadena;
		String fichero = "";
		
		FileReader f = new FileReader(path);
	    BufferedReader b = new BufferedReader(f);
	      while((cadena = b.readLine())!=null) {
	    	  fichero = fichero + cadena;
	    	  System.out.println(cadena);
	      }
	      b.close();
	          
	      return fichero;
	}
	


}
