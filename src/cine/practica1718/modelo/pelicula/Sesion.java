package cine.practica1718.modelo.pelicula;

public class Sesion {
	
	private int id, butacas;
	private String pase;
	
	
	public Sesion (int id, String pase, int butacas) {
		
		this.id = id;
		this.pase = pase;
		this.butacas = butacas;
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getButacas() {
		return butacas;
	}
	public void setButacas(int butacas) {
		this.butacas = butacas;
	}
	public String getPase() {
		return pase;
	}
	public void setPase(String pase) {
		this.pase = pase;
	}
	
	

}
