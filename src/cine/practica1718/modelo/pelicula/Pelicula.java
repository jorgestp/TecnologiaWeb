/**
 * 
 */
package cine.practica1718.modelo.pelicula;

/**
 *  *  @author 47536486V y 72978510Q
 *
 */
public class Pelicula {
	
	 private String nombre;
	 private int añopublicacion; //representa el año en el que la pelicula se estren�				 
	 private int id;
	 private double puntos;
	 private int sala;
	 private double precio;


	public Pelicula(String nombre, int añopublicacion) {

		this.nombre = nombre;
		this.añopublicacion = añopublicacion;
	}
	 
	 public Pelicula(int id, String nombre, int añopublicacion, double puntos,int sala, double precio) {
		 	
		 	this.id = id;
		 	this.nombre = nombre;
			this.añopublicacion = añopublicacion;
			
			this.puntos = puntos;
			this.sala = sala;
			this.precio = precio;
	 }


	public double getPuntos() {
		return puntos;
	}

	public void setPuntos(double puntos) {
		this.puntos = puntos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public int getAñopublicacion() {
		return añopublicacion;
	}

	public void setAñopublicacion(int añopublicacion) {
		this.añopublicacion = añopublicacion;
	}

	 
	 public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	 public int getSala() {
		return sala;
	}

	public void setSala(int sala) {
		this.sala = sala;
	}
	
	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

}
