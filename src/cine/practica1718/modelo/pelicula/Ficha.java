/**
 * 
 */
package cine.practica1718.modelo.pelicula;



/**
 *  *  @author 47536486V y 72978510Q
 *
 */
public class Ficha {

	private String  descripcion, categoria, actor, director;
	private int id;

	public Ficha(int id, String descripcion, String categoria, String actor, String director) {
		this.id = id;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.actor = actor;
		this.director = director;
	}

	public void setId( int id) {
		
		this.id = id;
	}
	
	public int getId() {
		
		return id;
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	
	
	
}
