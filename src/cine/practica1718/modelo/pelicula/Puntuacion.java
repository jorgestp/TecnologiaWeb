/**
 * 
 */
package cine.practica1718.modelo.pelicula;

/**
 * @author Windows
 *
 */
public class Puntuacion {
	
	private double puntuacion;
	private int contador;
	
	public Puntuacion (double punt) {
		
		generarPuntuacion(punt);
		
	}
	
	/*
	 * Metodo que har� la media aritmetica de la puntuacion que tenia anterior
	 * mas la nueva puntuacion
	 */
	
	private void generarPuntuacion(double nuevapuntuacion) {
		
		contador ++;
		puntuacion = (puntuacion + nuevapuntuacion)/contador;
		
	}
	
	public Double getpuntuacion() {
		
		return puntuacion;
	}


}
