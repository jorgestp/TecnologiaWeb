package cine.practica1718.modelo.pelicula;

import java.sql.*;
/**
 * Parte del modelo del patron.
 * 
 * Clase que hace accesos a la BBDD en relacion a las peliculas
 * 
 * 
 */
import java.util.ArrayList;
import java.util.List;

public class ControladorPelicula {
	
	
	private Connection conexion;
	
	/**
	 * Constructor que recibe por par�metro una conexion de la BBDD y se la pasa al campo de clase conexion
	 * @param conexion
	 */

	public ControladorPelicula(Connection conexion) {

		this.conexion = conexion;
	}
	
	
	/**
	 * Coge el campo conexion de la clase y accede con el a la BBDD, selecionando las peliculas 
	 * que estan registradas en la BBDD
	 * 
	 * @return lista de peliculas
	 */
	public List<Pelicula> getLista(){
		
		List<Pelicula> lista = new ArrayList<Pelicula>();
		
			try {
				Statement state = conexion.createStatement();
				
				String sql = "SELECT * FROM peliculas";
				
				ResultSet m = state.executeQuery(sql);
				
				while(m.next()) {
					
					int id = m.getInt(1);
					String nombre = m.getString(2);
					int anyo  = m.getInt(3);
					double puntos = m.getDouble(4);
					int sala = m.getInt(5);
					double precio = m.getDouble(6);
					Pelicula p =new Pelicula(id, nombre, anyo, puntos, sala, precio);
					lista.add(p);
				}
				
				m.close();
				state.close();
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		
		
		
		return lista;
	}


	public void insertaPelicula(Pelicula p) {
		
		PreparedStatement statepreparada = null;
		
		try {
			
			String sql ="INSERT INTO peliculas (ID, NOMBRE, ANO, PUNTUACION, SALA, PRECIO) VALUES (?,?,?,?,?,?)";
			
			statepreparada = conexion.prepareStatement(sql);
			
			statepreparada.setInt(1, p.getId());
			statepreparada.setString(2, p.getNombre());
			statepreparada.setInt(3, p.getAñopublicacion());
			statepreparada.setDouble(4, p.getPuntos());
			statepreparada.setInt(5, p.getSala());
			statepreparada.setDouble(6, p.getPrecio());
			
						
			statepreparada.executeUpdate();
			
			
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}


	public Pelicula getPelicula(String id) {
		
		PreparedStatement consultapreparada = null;
		
		Pelicula p = null;
		
		try {
			
			String sql="SELECT * FROM peliculas where ID=?";
			consultapreparada = conexion.prepareStatement(sql);
			consultapreparada.setString(1, id);
			ResultSet mr= consultapreparada.executeQuery();
			
			while(mr.next()) {
				
				p = new Pelicula(mr.getInt(1), mr.getString(2), mr.getInt(3), mr.getDouble(4), mr.getInt(5), mr.getDouble(6));
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return p;
	}


	public void actualizarButacas(String id, String seleccion, int butacasActuales) {
		
		int butacasActualizadas = butacasActuales - (Integer.parseInt(seleccion));
		PreparedStatement consultapreparada = null;
		
		try {
			
		String sql ="UPDATE peliculas SET BUTACAS=? WHERE ID=?"; 
		
		consultapreparada=conexion.prepareStatement(sql);
		
		consultapreparada.setInt(1, butacasActualizadas);
		consultapreparada.setInt(2, Integer.parseInt(id));
		
		consultapreparada.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}


	public boolean actualizarPuntuacionPelicula(String id, double d, double puntosActuales) {
		
		double nuevaPunt = (puntosActuales + d)/2;
		PreparedStatement consultapreparada = null;
		
		try {
			
		String sql ="UPDATE peliculas SET PUNTUACION=? WHERE ID=?"; 
		
		consultapreparada=conexion.prepareStatement(sql);
		
		consultapreparada.setDouble(1, nuevaPunt);
		consultapreparada.setInt(2, Integer.parseInt(id));
		
		consultapreparada.executeUpdate();
		return true;
		
		}catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		
	}


	public boolean actualizarSala(String id, String seleccion) {
		
		PreparedStatement consultapreparada = null;
		
		try {
			
		String sql ="UPDATE peliculas SET SALA=? WHERE ID=?"; 
		
		consultapreparada=conexion.prepareStatement(sql);
		
		consultapreparada.setDouble(1, Integer.parseInt(seleccion));
		consultapreparada.setInt(2, Integer.parseInt(id));
		
		consultapreparada.executeUpdate();
		return true;
		
		}catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		
		
	}


	public boolean actualizarPrecio(String id, String precio) {
		double nuevoprecio = Double.parseDouble(precio);
		PreparedStatement consultapreparada = null;
		
		try {
			
		String sql ="UPDATE peliculas SET PRECIO=? WHERE ID=?"; 
		
		consultapreparada=conexion.prepareStatement(sql);
		
		consultapreparada.setDouble(1, nuevoprecio);
		consultapreparada.setInt(2, Integer.parseInt(id));
		
		consultapreparada.executeUpdate();
		return true;
		
		}catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}

}


	public List<Pelicula> getPeliculaPorPuntuacion(String puntuacion) {
		
		int puntos = Integer.parseInt(puntuacion);
		
		if( puntos < 5) {
			
			String sql ="SELECT * FROM peliculas WHERE PUNTUACION < ?";
			
			return getPeliculaPorPuntuacion(puntos , sql);
		}
		
		if(puntos > 7.5) {
			
			String sql ="SELECT * FROM peliculas WHERE PUNTUACION > ?";
			return getPeliculaPorPuntuacion(puntos , sql);
		}
		
		String sql ="SELECT * FROM peliculas WHERE PUNTUACION BETWEEN ? AND ?";
		return getPeliculaPuntuacionEntre2Valores(puntos, 7.5, sql);
	}
	
	



	private List<Pelicula> getPeliculaPuntuacionEntre2Valores(double puntos, double puntos2, String s) {
		
		List<Pelicula> lista = new ArrayList<Pelicula>();
		PreparedStatement consulta = null;
		
		try {
			
			String sql=s;
			consulta = conexion.prepareStatement(sql);
			consulta.setDouble(1, puntos);
			consulta.setDouble(2, puntos2);
			
			ResultSet m = consulta.executeQuery();
			
			while(m.next()) {
				
			Pelicula pelicula = new Pelicula(m.getInt(1), m.getString(2), m.getInt(3), m.getDouble(4), m.getInt(5), m.getDouble(6));
			lista.add(pelicula);
			}
					
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return lista;
	}
	


	private List<Pelicula> getPeliculaPorPuntuacion(int puntos, String s){
		
		List<Pelicula> lista = new ArrayList<Pelicula>();
		PreparedStatement consulta = null;
		
		try {
			
			String sql=s;
			consulta = conexion.prepareStatement(sql);
			consulta.setInt(1, puntos);
			ResultSet m = consulta.executeQuery();
			
			while(m.next()) {
				
			Pelicula pelicula = new Pelicula(m.getInt(1), m.getString(2), m.getInt(3), m.getDouble(4), m.getInt(5), m.getDouble(6));
			lista.add(pelicula);
			}
					
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return lista;
	}
	
	public List<Pelicula> getPeliculaPuntuacionCategoria(String puntuacion, String categoria){
		
		List<Pelicula> lista = new ArrayList<Pelicula>();
		
		PreparedStatement consulta = null;
		try {
			
			if(Integer.parseInt(puntuacion)==8) {
				
				//peliculas con una categoria un puntacion mayor que 7.5
				String sql = "SELECT ID, NOMBRE, ANO, PUNTUACION, SALA, PRECIO  FROM PELICULAS , FICHAS  WHERE " + 
						"PELICULAS.ID = FICHAS.ID " + 
						"AND PELICULAS.PUNTUACION > ? " + 
						"AND FICHAS.CATEGORIA = ? ";
				consulta = conexion.prepareStatement(sql);
				consulta.setDouble(1, 7.5);
				consulta.setString(2, categoria);
				
				ResultSet r = consulta.executeQuery();
				
				while(r.next()) {
					
				Pelicula p = new Pelicula(r.getInt(1), r.getString(2), r.getInt(3), r.getDouble(4), r.getInt(5), r.getDouble(6));
					
				lista.add(p);
				}
				
				return lista;
				
			}else if(Integer.parseInt(puntuacion)<8 && Integer.parseInt(puntuacion)>5) {
				
				//peliculas con una categoria un puntacion entre que  5 y 7.5
				
				String sql = "SELECT ID, NOMBRE, A�O, PUNTUACION, SALA, PRECIO  FROM PELICULAS , FICHAS  WHERE " + 
						"PELICULAS.ID = FICHAS.ID " + 
						"AND PELICULAS.PUNTUACION BETWEEN ? AND ? " + 
						"AND FICHAS.CATEGORIA = ? ";
				consulta = conexion.prepareStatement(sql);
				consulta.setDouble(1, 5.0);
				consulta.setDouble(2, 7.5);
				consulta.setString(3, categoria);
				
				ResultSet r = consulta.executeQuery();
				
				while(r.next()) {
					
				Pelicula p = new Pelicula(r.getInt(1), r.getString(2), r.getInt(3), r.getDouble(4), r.getInt(5), r.getDouble(6));
					
				lista.add(p);
				}
				
				return lista;
				
			}else {
				
				//peliculas con una categoria un puntacion menor que 5
				String sql = "SELECT ID, NOMBRE, A�O, PUNTUACION, SALA, PRECIO  FROM PELICULAS , FICHAS  WHERE " + 
						"PELICULAS.ID = FICHAS.ID " + 
						"AND PELICULAS.PUNTUACION < ? " + 
						"AND FICHAS.CATEGORIA = ? ";
				consulta = conexion.prepareStatement(sql);
				consulta.setDouble(1, 5.0);
				consulta.setString(2, categoria);
				
				ResultSet r = consulta.executeQuery();
				
				while(r.next()) {
					
				Pelicula p = new Pelicula(r.getInt(1), r.getString(2), r.getInt(3), r.getDouble(4), r.getInt(5), r.getDouble(6));
					
				lista.add(p);
				}
				
				return lista;
				
			}
			
		}catch (Exception e) {

			e.printStackTrace();
		}
		
		

		return null;
	}

}
