package cine.practica1718.modelo.pelicula;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ControladorSesion {
	
	Connection conexion;
	
	public ControladorSesion(Connection conexion) {
		
		this.conexion = conexion;
	}
	
	public void insertarSesion(Sesion sesion) {
		
		PreparedStatement statepreparada = null;
		
		try {
			
			String sql ="INSERT INTO sesion (ID, PASE, BUTACAS) VALUES (?,?,?)";
			
			statepreparada = conexion.prepareStatement(sql);
			statepreparada.setInt(1, sesion.getId());
			statepreparada.setString(2, sesion.getPase());
			statepreparada.setInt(3, sesion.getButacas());
						
			statepreparada.executeUpdate();
			
			
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	public List<Sesion> getSalasPelicula(String id) {
		
		PreparedStatement consultapreparada = null;
		List<Sesion> lista_sesiones = new ArrayList<Sesion>();
		
				try {
			
					String sql="SELECT * FROM sesion where ID=?";
					consultapreparada = conexion.prepareStatement(sql);
					consultapreparada.setString(1, id);
					ResultSet mr= consultapreparada.executeQuery();
			
						while(mr.next()) {
							
							Sesion s = new Sesion(mr.getInt(1), mr.getString(2), mr.getInt(3));
							lista_sesiones.add(s);
						}
			
			
					}catch(Exception e) {
						e.printStackTrace();
					}

		return lista_sesiones;
	}

	public Sesion getSesionPelicula(String id, String seleccion) {
		
		Sesion sesion = null;
		
		PreparedStatement consultapreparada = null;

		
				try {
			
					String sql="SELECT * FROM sesion where ID=? and PASE=?";
					consultapreparada = conexion.prepareStatement(sql);
					consultapreparada.setString(1, id);
					consultapreparada.setString(2, seleccion);
					ResultSet mr= consultapreparada.executeQuery();
			
						while(mr.next()) {
							
							sesion = new Sesion(mr.getInt(1), mr.getString(2), mr.getInt(3));
							
						}
			
			
					}catch(Exception e) {
						e.printStackTrace();
					}
		
		return sesion;
	}

	public void actualizaButaca(String id, String ButacasSeleccionadas, String pase, int butacasActuales) {
		
		int butacasActualizadas = butacasActuales - (Integer.parseInt(ButacasSeleccionadas));
		PreparedStatement consultapreparada = null;
		
		try {
			
		String sql ="UPDATE sesion SET BUTACAS=? WHERE ID=? AND PASE=?"; 
		
		consultapreparada=conexion.prepareStatement(sql);
		
		consultapreparada.setInt(1, butacasActualizadas);
		consultapreparada.setInt(2, Integer.parseInt(id));
		consultapreparada.setString(3, pase);
		
		consultapreparada.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void actualizarAsientos(String id, String B_seleccion, String pase, int ButacasActuales) {
		
		PreparedStatement consultapreparada = null;
		
		try {
			String sql = "UPDATE SESION SET BUTACAS=? WHERE ID=? AND PASE=?";
			consultapreparada = conexion.prepareStatement(sql);
			consultapreparada.setInt(1, (ButacasActuales - (Integer.parseInt(B_seleccion))));
			consultapreparada.setString(2, id);
			consultapreparada.setString(3, pase);
			
			consultapreparada.executeUpdate();
			
		}catch (Exception e) {
			
		}
	}
	
	public String getDiaDeLaSemana() {
		
		String [] dia = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
		
		Calendar now = Calendar.getInstance();
		return dia[now.get(Calendar.DAY_OF_WEEK)-1];
	}
		
	
	

}
