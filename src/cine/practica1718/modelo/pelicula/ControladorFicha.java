package cine.practica1718.modelo.pelicula;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;



public class ControladorFicha {

	private Connection conexion;

	public ControladorFicha(Connection conexion) {
		
		this.conexion = conexion;
	}

	public Ficha getFicha(String id) {
		
		PreparedStatement consultapreparada = null;
		Ficha ficha =null;
		
		try {
			
			String sql = "SELECT * FROM FICHAS WHERE ID=?";
			consultapreparada = conexion.prepareStatement(sql);
			consultapreparada.setInt(1, Integer.parseInt(id));
			ResultSet r = consultapreparada.executeQuery();
			
			while(r.next()) {
				
				ficha = new Ficha(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getString(5));
			}
			
		}catch (Exception e) {
			
		}
		
		return ficha;
	}

	public void insertarFicha(Ficha ficha) {
		
		PreparedStatement consultapreparada = null;
		
		try {
			
			String sql = "INSERT INTO FICHAS (ID, DESCRIPCION, CATEGORIA, ACTOR, DIRECTOR) VALUES (?,?,?,?,?)";
			consultapreparada = conexion.prepareStatement(sql);
			
			consultapreparada.setInt(1, ficha.getId());
			consultapreparada.setString(2, ficha.getDescripcion());
			consultapreparada.setString(3, ficha.getCategoria());
			consultapreparada.setString(4, ficha.getActor());
			consultapreparada.setString(5, ficha.getDirector());
			
			consultapreparada.executeUpdate();
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

	public List<Ficha> getCategoriayActor(String categoria, String actor) {
		
		List<Ficha> lista = new ArrayList<Ficha>();
		
		PreparedStatement consulta = null;
		
		try {
			
			String sql = "SELECT * FROM FICHAS WHERE CATEGORIA=? AND ACTOR=?";
			consulta = conexion.prepareStatement(sql);
			consulta.setString(1, categoria);
			consulta.setString(2, actor);
			
			ResultSet result = consulta.executeQuery();
			
			while(result.next()) {
				
				Ficha ficha = new Ficha(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5));
				lista.add(ficha);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return lista;
	}

	public List<Ficha> getFichaCategoria(String categoria) {
		
		List<Ficha> lista = new ArrayList<Ficha>();
		PreparedStatement consulta = null;
		
		try {
			
			String sql = "SELECT * FROM FICHAS WHERE CATEGORIA=?";
			consulta = conexion.prepareStatement(sql);
			consulta.setString(1, categoria);
			ResultSet r = consulta.executeQuery();
			
			while(r.next()) {
				
				Ficha ficha = new Ficha(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getString(5));
				lista.add(ficha);
			}
			
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return lista;
	}

	public List<Ficha> getListaPorActor(String actor) {
		
		List<Ficha> list = new ArrayList<Ficha>();
		PreparedStatement consulta = null;
		
		try {
			
			String sql = "SELECT * FROM FICHAS WHERE ACTOR = ?";
			consulta = conexion.prepareStatement(sql);
			consulta.setString(1, actor);
			
			ResultSet r = consulta.executeQuery();
			
			while(r.next()) {
				
				Ficha f =new Ficha(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), r.getString(5));
				list.add(f);
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return list;
	}
	
	


	
	
}
