<<<<<<< HEAD
package cine.practica1718.controlador;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import java.nio.file.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.jni.File;

import cine.practica1718.modelo.pelicula.*;
import cine.practica1718.modelo.usuario.Cliente;
import cine.practica1718.modelo.usuario.ControladorCliente;

/**
 * @author Windows
 * 
 * Clase que hace de unico controlador, implementando un servlet
 */
@WebServlet("/ManagerCine")
public class ManagerCine extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection conexion = null;
	
	private ControladorPelicula cont_pelicula;
	private ControladorCliente cont_cliente;
	private ControladorFicha cont_ficha;
	private ControladorSesion cont_sesion;
	private ControladorBBDD cont_BBDD;
	private List<Pelicula> list =null;
	private HttpSession sesion =null;
         
	public ManagerCine() {
        
    }

	/**
	 * 
	 * Se trata del primer m�todo que ejecuta la aplicacion cuando se inicia por primera vez
	 */
	public void init(ServletConfig config) throws ServletException {						
		conexion = getConecction();
		cont_pelicula = new ControladorPelicula(conexion);
		cont_cliente = new ControladorCliente(conexion);
		cont_ficha = new ControladorFicha(conexion);
		cont_sesion = new ControladorSesion(conexion);
		cont_BBDD = new ControladorBBDD(conexion); 
							
		try {
			cont_BBDD.loadScript(config.getServletContext().getRealPath("WEB-INF\\sql\\Cine.script"));
			cont_BBDD.loadScript(config.getServletContext().getRealPath("WEB-INF\\sql\\Value_Default.script"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * M�todo que se encarga de obtener las peticiones v�a get de los usuarios mediante el 
	 * parametro request, y devuelve un resultado con response
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String comando = request.getParameter("instruccion");
		
		if(comando ==null) {
			
			comando = "listar";
		}
		
		switch (comando) {
		case "listar":
				obtenerlista(request, response);			
			break;
		
		case "registroUsuario":
				agregarCliente(request, response);				
			break;
		case "login":
				accederUsuario(request, response);			
			break;
		case "insertaPelicula":
				insertarPelicula(request, response);
			break;
		case "formalizarSesiones":
				formalizarSesiones(request, response);
			break;
		case "formalizarInserccionPelicula":
				formalizarInserccionPelicula(request, response);
			break;
		case "comprasEntradas":
				accederCompras(request, response);
			break;
		case "entradas":
				comprar(request, response);
			break;
		case "formalizarCompra":
				formalizarcompra(request, response);
			break;
		case "realizarCompra":
				realizarCompra(request, response);
			break;
		case "puntuarPelicula":
			puntuarPelicula(request, response);
			break;
		case "puntos":
			puntos(request, response);
			break;
		case "formalizarPuntuacion":
			realizarPuntuacion(request, response);
			break;
		case "salas":
			salas(request, response);
			break;
		case "accesoconfiguracionsala":
			accesoconfiguracionsala(request, response);
			break;
		case "formalizarSala":
			formalizarSala(request, response);
			break;
		case "precio":
			mostrarlinkPrecio(request, response);
			break;
		case "accesoconfiguracionprecio":
			accesoconfiguracionPrecio(request, response);
			break;
		case "formalizarPrecio":
			formalizarPrecio(request, response);
			break;
		case "salirLogin":
			salirLogin(request,response);
			break;
		case "baja":
			bajacliente(request,response);
			break;
		case "verficha":
			verficha(request, response);
			break;
		case "vueltaInicioCliente":
			vueltaInicioCliente(request, response);
			break;
		case "vueltaInicioAdmin":
			vueltaInicioAdmin(request, response);
			break;
		case "formalizarFicha":
			formalizarInserccionFicha(request, response);
			break;
		case "filtrar":
			filtrado(request, response);
		}

			

		
	}
	


	private void filtrado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String categoria = request.getParameter("categoria");
		String actor = request.getParameter("actor");
		String puntuacion = request.getParameter("puntuacion");
		
		// categoria diferente a defecto, actor defecto y puntuacion defecto ; filtra por categoria solo
		if(!categoria.equals("Categoria") && actor.equals("") && puntuacion.equals("0")) {
			
			List<Ficha> lista_fichas = cont_ficha.getFichaCategoria(categoria);
			List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
			
			for(Ficha f:lista_fichas) {
				
				lista_pelicula.add(cont_pelicula.getPelicula(Integer.toString(f.getId())));
				
			}
			
			if(!lista_pelicula.isEmpty()) {
				
				request.setAttribute("LISTAPELI", lista_pelicula);
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
				
				dispatcher.forward(request, response);
				
			}

			
			
		}else 
			//categoria difetente a defecto, actor diferente defecto y puntuacion defecto; filtra categoria y actor
			if(!categoria.equals("Categoria") && !actor.equals("") && puntuacion.equals("0")) {
		
				List<Ficha> lista_fichas = cont_ficha.getCategoriayActor(categoria, actor);
				
				List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
				for(Ficha f: lista_fichas) {
										
					Pelicula pelicula = cont_pelicula.getPelicula(Integer.toString(f.getId()));
					
					lista_pelicula.add(pelicula);
				}
				
				if(!lista_pelicula.isEmpty()) {
					
					request.setAttribute("LISTAPELI", lista_pelicula);
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
					
					dispatcher.forward(request, response);
					
				}else {
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
					
					dispatcher.forward(request, response);
					
				}
				
			}else
				//categoria  defecto, actor  defecto y puntuacion diferente defecto; filtra puntuacion solo
				if(categoria.equals("Categoria") && actor.equals("") && !puntuacion.equals("0")) {
					
					List<Pelicula> lista_pelicula = cont_pelicula.getPeliculaPorPuntuacion(puntuacion);
					request.setAttribute("LISTAPELI", lista_pelicula);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
					
					dispatcher.forward(request, response);
					
				}else  
					//categoria diferente defecto, actor  defecto y puntuacion diferente defecto; filtra puntuacion y categoria
					if(!categoria.equals("Categoria") && actor.equals("") && !puntuacion.equals("0")){
					
						
						List<Pelicula> lista_pelicula =cont_pelicula.getPeliculaPuntuacionCategoria(puntuacion, categoria);
						
						if(!lista_pelicula.isEmpty()) {
							
							request.setAttribute("LISTAPELI", lista_pelicula);
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
							
							dispatcher.forward(request, response);
							
						}else {
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
							
							dispatcher.forward(request, response);
							
						}
						
						
					
					}else
						//categoria  defecto, actor diferente defecto y puntuacion  defecto; filtra actor solo solo
						if(categoria.equals("Categoria") && !actor.equals("") && puntuacion.equals("0")) {
							
							List<Ficha> lista_por_actor = cont_ficha.getListaPorActor(actor);
							
							List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
							
							for(Ficha f : lista_por_actor) {
								
								lista_pelicula.add(cont_pelicula.getPelicula(Integer.toString(f.getId())));
							}
							if(!lista_pelicula.isEmpty()) {
								
								request.setAttribute("LISTAPELI", lista_pelicula);
								
								RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
								
								dispatcher.forward(request, response);
								
							}else {
								
								RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
								
								dispatcher.forward(request, response);
								
							}
							
							
						}else {
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/ErrorFiltros.jsp");
							
							dispatcher.forward(request, response);
						}
		
		
		
		
		
		
		
	}

	private void vueltaInicioAdmin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		list = cont_pelicula.getLista();
		
		request.setAttribute("LISTAPELI", list);			
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Administrador.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void vueltaInicioCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("LISTAPELI", list);			
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/cliente.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void verficha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		Ficha ficha = cont_ficha.getFicha(id);
		
		request.setAttribute("ficha", ficha);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/fichaPelicula.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void bajacliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String user = request.getParameter("usuario");
		String pass = request.getParameter("contra");
		
		if(!user.equals("admin")) {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null) {
							
				cont_cliente.darBajaCliente(user, pass);
				request.setAttribute("LISTAPELI", list);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteExito.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteError.jsp");
				
				dispatcher.forward(request, response);
				
			}				
		}else {
			

				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteAdmin.jsp");
				
				dispatcher.forward(request, response);				
				
			}
		
		
		
	}

	private void salirLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		sesion.invalidate();
		request.setAttribute("LISTAPELI", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
		
	}
	
	

	private void formalizarPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String precio = request.getParameter("precio");
		
		if(cont_pelicula.actualizarPrecio(id, precio)) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			request.setAttribute("seleccion", precio);
			list = cont_pelicula.getLista();
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionPrecioExito.jsp");
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionPrecioError.jsp");
			dispatcher.forward(request, response);
		}
		

		
	}

	private void accesoconfiguracionPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/configuracionPrecio.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void mostrarlinkPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/accesoPrecio.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void formalizarSala(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
		
		if(cont_pelicula.actualizarSala(id, seleccion)) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			
			request.setAttribute("seleccion", seleccion);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionSalaExito.jsp");
			
			dispatcher.forward(request, response);
			
		}else {
			
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionSalaError.jsp");
			
			dispatcher.forward(request, response);
		}
		
		

		
		
	}

	private void accesoconfiguracionsala(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/configuracionSala.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void salas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/accesoSalas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void insertarPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nombre = request.getParameter("nombre");
		int anyo = Integer.parseInt(request.getParameter("anyo"));
		Pelicula p = new Pelicula(id ,nombre, anyo, 5.0, 0, 4.5);
		
		cont_pelicula.insertaPelicula(p);
		list = cont_pelicula.getLista();
		request.setAttribute("pelicula", p);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_Sesiones.jsp");
		dispatcher.forward(request, response);
		
	}
	
	
	private void formalizarSesiones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] sesionespeliculas = request.getParameterValues("articulos");
		int id = Integer.parseInt(request.getParameter("id"));
		Pelicula pelicula = cont_pelicula.getPelicula(request.getParameter("id"));
		
		for(String s : sesionespeliculas) {
			
			Sesion sesion = new Sesion(id, s, 45);
			
			cont_sesion.insertarSesion(sesion);

		}
		
		request.setAttribute("pelicula", pelicula);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_Ficha.jsp");
		dispatcher.forward(request, response);
	}
	
	
	private void formalizarInserccionFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String descripcion = request.getParameter("descripcion");
		String categoria = request.getParameter("categoria");
		String actor = request.getParameter("actor");
		String director = request.getParameter("director");
		
		Ficha ficha = new Ficha(Integer.parseInt(id), descripcion, categoria, actor, director);
		
		cont_ficha.insertarFicha(ficha);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/pruebaInserrcionFicha.jsp");
		dispatcher.forward(request, response);
	}

	
		
	

	private void realizarPuntuacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
			
		Pelicula p = cont_pelicula.getPelicula(id);
		
		
			
		if(cont_pelicula.actualizarPuntuacionPelicula(id, Double.parseDouble(seleccion), p.getPuntos())) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			request.setAttribute("seleccion", seleccion);
			list = cont_pelicula.getLista();
			RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacionExito.jsp");
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacionError.jsp");
			dispatcher.forward(request, response);
		}

		
	}

	private void puntos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String id = request.getParameter("id");
	
		Pelicula p = cont_pelicula.getPelicula(id);
			
		request.setAttribute("pelicula", p);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacion.jsp");
	
		dispatcher.forward(request, response);
		
	}



	private void puntuarPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuarPelicula.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void formalizarcompra(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
		String pase = request.getParameter("pase");
			
		Sesion sesion = cont_sesion.getSesionPelicula(id, pase);
		
		if(sesion.getButacas()>=Integer.parseInt(seleccion)) {
			
			cont_sesion.actualizarAsientos(id, seleccion, pase, sesion.getButacas());
			
			
			sesion = cont_sesion.getSesionPelicula(id, pase);
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			
			request.setAttribute("sesion", sesion);
			request.setAttribute("pelicula",pelicula);
			
			request.setAttribute("seleccion", seleccion);			
			String dia = cont_sesion.getDiaDeLaSemana();
			request.setAttribute("dia", dia );
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/CompraExito.jsp");
			
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/CompraError.jsp");
			
			dispatcher.forward(request, response);	
			
		}
	
	}
	
	private void realizarCompra(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String pase = request.getParameter("sesion");
		String id = request.getParameter("id");
		Pelicula pelicula = cont_pelicula.getPelicula(id);
		Sesion s = cont_sesion.getSesionPelicula(id, pase);
		
		request.setAttribute("pelicula", pelicula);
		
		request.setAttribute("sesion", s);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/realizarCompra.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void comprar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
		
		List<Sesion> lista_sesiones = cont_sesion.getSalasPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		request.setAttribute("sesiones", lista_sesiones);
		request.setAttribute("tama�oSesiones", lista_sesiones.size());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/entradas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void accederCompras(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/comprasEntradas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	/**
	 * Metodo para comprobar que acceder con la clave y el usuario al sitio web
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	
	private void accederUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String user = request.getParameter("usuario");
		String pass = request.getParameter("contra");
		sesion = request.getSession();
		
		if(!user.equals("admin")) {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null && sesion.getAttribute("usuario") == null) {
				list = cont_pelicula.getLista();
				request.setAttribute("LISTAPELI", list);			
				sesion.setAttribute("usuario", clienteSeccion.getNombre());
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/cliente.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/clienteError.jsp");
				
				dispatcher.forward(request, response);
				
			}				
		}else {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null && sesion.getAttribute("usuario")==null) {
				list = cont_pelicula.getLista();
				request.setAttribute("LISTAPELI", list);			
				sesion.setAttribute("usuario", clienteSeccion.getNombre());
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/Administrador.jsp");
				
				dispatcher.forward(request, response);
								
			}else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/clienteError.jsp");
				
				dispatcher.forward(request, response);				
				
			}
		}
		
	}

	/**
	 * Metodo que se encarga de recoger los datos del jsp, crea un cliente y lo manda al modelo del cliente
	 * para agregarlo a la BBDD
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	
	private void agregarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// almacenan en cada String los valores introducidos en el teclado
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String user = request.getParameter("usuario");
		String pss = request.getParameter("contra");
		
		Cliente cliente = new Cliente(nombre, apellido, user, pss);
		
		if(cont_cliente.agregaCliente(cliente)) {
			
			request.setAttribute("LISTAPELI", list);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/registroExito.jsp");
			
			dispatcher.forward(request, response);
			
			
		}else {
			
			
		}
		
	}

	private void formalizarInserccionPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_pelicula.jsp");
		
		dispatcher.forward(request, response);

	}
	/**
	 * 
	 * M�todo que accede al modelo de la clase Pel�cula (controladorPelicula) para pedirle la lista de peliculas
	 * y devolverlas a la vista prueba.jsp
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void obtenerlista(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		list = cont_pelicula.getLista();
		
		request.setAttribute("LISTAPELI", list);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		
		dispatcher.forward(request, response);		
	}
	/**
	 * M�todo que se encarga de cargar el driver de la BBDD y realizar la conexi�n, devolviendo
	 * un objeto de esa conexi�n
	 * 
	 * @return objeto de la Interfaz java.sql.Connection
	 */

	private Connection getConecction() {
		
		Connection c = null;
		
		try {
			
		Class.forName("org.hsqldb.jdbcDriver");
		c = DriverManager.getConnection("jdbc:hsqldb:mem:cineDB", "sa", "1234");		
		
		System.out.println("En linea");
		
		} catch (Exception e) {
	
			System.out.println("Error" + e.getMessage());
		}
		
		return c;
	}
	

	
	


}
=======
package cine.practica1718.controlador;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;



import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import cine.practica1718.modelo.pelicula.*;
import cine.practica1718.modelo.usuario.Cliente;
import cine.practica1718.modelo.usuario.ControladorCliente;

/**
 * @author Windows
 * 
 * Clase que hace de unico controlador, implementando un servlet
 */
@WebServlet("/ManagerCine")
public class ManagerCine extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection conexion = null;
	
	private ControladorPelicula cont_pelicula;
	private ControladorCliente cont_cliente;
	private ControladorFicha cont_ficha;
	private ControladorSesion cont_sesion;
	private ControladorBBDD cont_BBDD;
	private List<Pelicula> list =null;
	private HttpSession sesion =null;
         
	public ManagerCine() {
        
    }

	/**
	 * 
	 * Se trata del primer m�todo que ejecuta la aplicacion cuando se inicia por primera vez
	 */
	public void init(ServletConfig config) throws ServletException {						
		conexion = getConecction();
		cont_pelicula = new ControladorPelicula(conexion);
		cont_cliente = new ControladorCliente(conexion);
		cont_ficha = new ControladorFicha(conexion);
		cont_sesion = new ControladorSesion(conexion);
		cont_BBDD = new ControladorBBDD(conexion); 
			
		try {
			System.out.println(new java.io.File(".").getCanonicalPath());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			cont_BBDD.loadScript(config.getServletContext().getRealPath("cine.sql"));
			cont_BBDD.loadScript(config.getServletContext().getRealPath("value_Default.sql"));
			
			//cont_BBDD.loadScript(config.getServletContext().getRealPath("WEB-INF\\sql\\Value_Default.script"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * M�todo que se encarga de obtener las peticiones v�a get de los usuarios mediante el 
	 * parametro request, y devuelve un resultado con response
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String comando = request.getParameter("instruccion");
		
		if(comando ==null) {
			
			comando = "listar";
		}
		
		switch (comando) {
		case "listar":
				obtenerlista(request, response);			
			break;
		
		case "registroUsuario":
				agregarCliente(request, response);				
			break;
		case "login":
				accederUsuario(request, response);			
			break;
		case "insertaPelicula":
				insertarPelicula(request, response);
			break;
		case "formalizarSesiones":
				formalizarSesiones(request, response);
			break;
		case "formalizarInserccionPelicula":
				formalizarInserccionPelicula(request, response);
			break;
		case "comprasEntradas":
				accederCompras(request, response);
			break;
		case "entradas":
				comprar(request, response);
			break;
		case "formalizarCompra":
				formalizarcompra(request, response);
			break;
		case "realizarCompra":
				realizarCompra(request, response);
			break;
		case "puntuarPelicula":
			puntuarPelicula(request, response);
			break;
		case "puntos":
			puntos(request, response);
			break;
		case "formalizarPuntuacion":
			realizarPuntuacion(request, response);
			break;
		case "salas":
			salas(request, response);
			break;
		case "accesoconfiguracionsala":
			accesoconfiguracionsala(request, response);
			break;
		case "formalizarSala":
			formalizarSala(request, response);
			break;
		case "precio":
			mostrarlinkPrecio(request, response);
			break;
		case "accesoconfiguracionprecio":
			accesoconfiguracionPrecio(request, response);
			break;
		case "formalizarPrecio":
			formalizarPrecio(request, response);
			break;
		case "salirLogin":
			salirLogin(request,response);
			break;
		case "baja":
			bajacliente(request,response);
			break;
		case "verficha":
			verficha(request, response);
			break;
		case "vueltaInicioCliente":
			vueltaInicioCliente(request, response);
			break;
		case "vueltaInicioAdmin":
			vueltaInicioAdmin(request, response);
			break;
		case "formalizarFicha":
			formalizarInserccionFicha(request, response);
			break;
		case "filtrar":
			filtrado(request, response);
		}

			

		
	}
	


	private void filtrado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String categoria = request.getParameter("categoria");
		String actor = request.getParameter("actor");
		String puntuacion = request.getParameter("puntuacion");
		
		// categoria diferente a defecto, actor defecto y puntuacion defecto ; filtra por categoria solo
		if(!categoria.equals("Categoria") && actor.equals("") && puntuacion.equals("0")) {
			
			List<Ficha> lista_fichas = cont_ficha.getFichaCategoria(categoria);
			List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
			
			for(Ficha f:lista_fichas) {
				
				lista_pelicula.add(cont_pelicula.getPelicula(Integer.toString(f.getId())));
				
			}
			
			if(!lista_pelicula.isEmpty()) {
				
				request.setAttribute("LISTAPELI", lista_pelicula);
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
				
				dispatcher.forward(request, response);
				
			}

			
			
		}else 
			//categoria difetente a defecto, actor diferente defecto y puntuacion defecto; filtra categoria y actor
			if(!categoria.equals("Categoria") && !actor.equals("") && puntuacion.equals("0")) {
		
				List<Ficha> lista_fichas = cont_ficha.getCategoriayActor(categoria, actor);
				
				List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
				for(Ficha f: lista_fichas) {
										
					Pelicula pelicula = cont_pelicula.getPelicula(Integer.toString(f.getId()));
					
					lista_pelicula.add(pelicula);
				}
				
				if(!lista_pelicula.isEmpty()) {
					
					request.setAttribute("LISTAPELI", lista_pelicula);
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
					
					dispatcher.forward(request, response);
					
				}else {
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
					
					dispatcher.forward(request, response);
					
				}
				
			}else
				//categoria  defecto, actor  defecto y puntuacion diferente defecto; filtra puntuacion solo
				if(categoria.equals("Categoria") && actor.equals("") && !puntuacion.equals("0")) {
					
					List<Pelicula> lista_pelicula = cont_pelicula.getPeliculaPorPuntuacion(puntuacion);
					request.setAttribute("LISTAPELI", lista_pelicula);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
					
					dispatcher.forward(request, response);
					
				}else  
					//categoria diferente defecto, actor  defecto y puntuacion diferente defecto; filtra puntuacion y categoria
					if(!categoria.equals("Categoria") && actor.equals("") && !puntuacion.equals("0")){
					
						
						List<Pelicula> lista_pelicula =cont_pelicula.getPeliculaPuntuacionCategoria(puntuacion, categoria);
						
						if(!lista_pelicula.isEmpty()) {
							
							request.setAttribute("LISTAPELI", lista_pelicula);
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
							
							dispatcher.forward(request, response);
							
						}else {
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
							
							dispatcher.forward(request, response);
							
						}
						
						
					
					}else
						//categoria  defecto, actor diferente defecto y puntuacion  defecto; filtra actor solo solo
						if(categoria.equals("Categoria") && !actor.equals("") && puntuacion.equals("0")) {
							
							List<Ficha> lista_por_actor = cont_ficha.getListaPorActor(actor);
							
							List<Pelicula> lista_pelicula = new ArrayList<Pelicula>();
							
							for(Ficha f : lista_por_actor) {
								
								lista_pelicula.add(cont_pelicula.getPelicula(Integer.toString(f.getId())));
							}
							if(!lista_pelicula.isEmpty()) {
								
								request.setAttribute("LISTAPELI", lista_pelicula);
								
								RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultado.jsp");
								
								dispatcher.forward(request, response);
								
							}else {
								
								RequestDispatcher dispatcher = request.getRequestDispatcher("/filtrarResultadoVacio.jsp");
								
								dispatcher.forward(request, response);
								
							}
							
							
						}else {
							
							RequestDispatcher dispatcher = request.getRequestDispatcher("/ErrorFiltros.jsp");
							
							dispatcher.forward(request, response);
						}
		
		
		
		
		
		
		
	}

	private void vueltaInicioAdmin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		list = cont_pelicula.getLista();
		
		request.setAttribute("LISTAPELI", list);			
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Administrador.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void vueltaInicioCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("LISTAPELI", list);			
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/cliente.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void verficha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		Ficha ficha = cont_ficha.getFicha(id);
		
		request.setAttribute("ficha", ficha);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/fichaPelicula.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void bajacliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String user = request.getParameter("usuario");
		String pass = request.getParameter("contra");
		
		if(!user.equals("admin")) {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null) {
							
				cont_cliente.darBajaCliente(user, pass);
				request.setAttribute("LISTAPELI", list);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteExito.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteError.jsp");
				
				dispatcher.forward(request, response);
				
			}				
		}else {
			

				RequestDispatcher dispatcher = request.getRequestDispatcher("/bajaClienteAdmin.jsp");
				
				dispatcher.forward(request, response);				
				
			}
		
		
		
	}

	private void salirLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		sesion.invalidate();
		request.setAttribute("LISTAPELI", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
		
	}
	
	

	private void formalizarPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String precio = request.getParameter("precio");
		
		if(cont_pelicula.actualizarPrecio(id, precio)) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			request.setAttribute("seleccion", precio);
			list = cont_pelicula.getLista();
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionPrecioExito.jsp");
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionPrecioError.jsp");
			dispatcher.forward(request, response);
		}
		

		
	}

	private void accesoconfiguracionPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/configuracionPrecio.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void mostrarlinkPrecio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/accesoPrecio.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void formalizarSala(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
		
		if(cont_pelicula.actualizarSala(id, seleccion)) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			
			request.setAttribute("seleccion", seleccion);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionSalaExito.jsp");
			
			dispatcher.forward(request, response);
			
		}else {
			
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/actualizacionSalaError.jsp");
			
			dispatcher.forward(request, response);
		}
		
		

		
		
	}

	private void accesoconfiguracionsala(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/configuracionSala.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void salas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/accesoSalas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void insertarPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nombre = request.getParameter("nombre");
		int anyo = Integer.parseInt(request.getParameter("anyo"));
		Pelicula p = new Pelicula(id ,nombre, anyo, 5.0, 0, 4.5);
		
		cont_pelicula.insertaPelicula(p);
		list = cont_pelicula.getLista();
		request.setAttribute("pelicula", p);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_Sesiones.jsp");
		dispatcher.forward(request, response);
		
	}
	
	
	private void formalizarSesiones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] sesionespeliculas = request.getParameterValues("articulos");
		int id = Integer.parseInt(request.getParameter("id"));
		Pelicula pelicula = cont_pelicula.getPelicula(request.getParameter("id"));
		
		for(String s : sesionespeliculas) {
			
			Sesion sesion = new Sesion(id, s, 45);
			
			cont_sesion.insertarSesion(sesion);

		}
		
		request.setAttribute("pelicula", pelicula);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_Ficha.jsp");
		dispatcher.forward(request, response);
	}
	
	
	private void formalizarInserccionFicha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String descripcion = request.getParameter("descripcion");
		String categoria = request.getParameter("categoria");
		String actor = request.getParameter("actor");
		String director = request.getParameter("director");
		
		Ficha ficha = new Ficha(Integer.parseInt(id), descripcion, categoria, actor, director);
		
		cont_ficha.insertarFicha(ficha);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/pruebaInserrcionFicha.jsp");
		dispatcher.forward(request, response);
	}

	
		
	

	private void realizarPuntuacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
			
		Pelicula p = cont_pelicula.getPelicula(id);
		
		
			
		if(cont_pelicula.actualizarPuntuacionPelicula(id, Double.parseDouble(seleccion), p.getPuntos())) {
			
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			request.setAttribute("pelicula",pelicula);
			request.setAttribute("seleccion", seleccion);
			list = cont_pelicula.getLista();
			RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacionExito.jsp");
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacionError.jsp");
			dispatcher.forward(request, response);
		}

		
	}

	private void puntos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String id = request.getParameter("id");
	
		Pelicula p = cont_pelicula.getPelicula(id);
			
		request.setAttribute("pelicula", p);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuacion.jsp");
	
		dispatcher.forward(request, response);
		
	}



	private void puntuarPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/puntuarPelicula.jsp");
		
		dispatcher.forward(request, response);
		
		
	}

	private void formalizarcompra(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String seleccion = request.getParameter("articulos");
		String pase = request.getParameter("pase");
			
		Sesion sesion = cont_sesion.getSesionPelicula(id, pase);
		
		if(sesion.getButacas()>=Integer.parseInt(seleccion)) {
			
			cont_sesion.actualizarAsientos(id, seleccion, pase, sesion.getButacas());
			
			
			sesion = cont_sesion.getSesionPelicula(id, pase);
			Pelicula pelicula = cont_pelicula.getPelicula(id);
			
			request.setAttribute("sesion", sesion);
			request.setAttribute("pelicula",pelicula);
			
			request.setAttribute("seleccion", seleccion);			
			String dia = cont_sesion.getDiaDeLaSemana();
			request.setAttribute("dia", dia );
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/CompraExito.jsp");
			
			dispatcher.forward(request, response);
			
		}else {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/CompraError.jsp");
			
			dispatcher.forward(request, response);	
			
		}
	
	}
	
	private void realizarCompra(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String pase = request.getParameter("sesion");
		String id = request.getParameter("id");
		Pelicula pelicula = cont_pelicula.getPelicula(id);
		Sesion s = cont_sesion.getSesionPelicula(id, pase);
		
		request.setAttribute("pelicula", pelicula);
		
		request.setAttribute("sesion", s);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/realizarCompra.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void comprar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
		String id = request.getParameter("id");
		
		Pelicula p = cont_pelicula.getPelicula(id);
		
		List<Sesion> lista_sesiones = cont_sesion.getSalasPelicula(id);
				
		request.setAttribute("pelicula", p);
		
		request.setAttribute("sesiones", lista_sesiones);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/entradas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	private void accederCompras(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("LISTAPELI", cont_pelicula.getLista());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/comprasEntradas.jsp");
		
		dispatcher.forward(request, response);
		
	}

	/**
	 * Metodo para comprobar que acceder con la clave y el usuario al sitio web
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	
	private void accederUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String user = request.getParameter("usuario");
		String pass = request.getParameter("contra");
		sesion = request.getSession();
		
		if(!user.equals("admin")) {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null && sesion.getAttribute("usuario") == null) {
				list = cont_pelicula.getLista();
				request.setAttribute("LISTAPELI", list);			
				sesion.setAttribute("usuario", clienteSeccion.getNombre());
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/cliente.jsp");
				
				dispatcher.forward(request, response);
				
			}else {
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/clienteError.jsp");
				
				dispatcher.forward(request, response);
				
			}				
		}else {
			
			Cliente cliente = new Cliente(user, pass);
			Cliente clienteSeccion = cont_cliente.accedeCliente(cliente);
			
			if(clienteSeccion!=null && sesion.getAttribute("usuario")==null) {
				list = cont_pelicula.getLista();
				request.setAttribute("LISTAPELI", list);			
				sesion.setAttribute("usuario", clienteSeccion.getNombre());
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/Administrador.jsp");
				
				dispatcher.forward(request, response);
								
			}else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/clienteError.jsp");
				
				dispatcher.forward(request, response);				
				
			}
		}
		
	}

	/**
	 * Metodo que se encarga de recoger los datos del jsp, crea un cliente y lo manda al modelo del cliente
	 * para agregarlo a la BBDD
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	
	private void agregarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// almacenan en cada String los valores introducidos en el teclado
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String user = request.getParameter("usuario");
		String pss = request.getParameter("contra");
		
		Cliente cliente = new Cliente(nombre, apellido, user, pss);
		
		if(cont_cliente.agregaCliente(cliente)) {
			
			request.setAttribute("LISTAPELI", list);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/registroExito.jsp");
			
			dispatcher.forward(request, response);
			
			
		}else {
			
			
		}
		
	}

	private void formalizarInserccionPelicula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/insertar_pelicula.jsp");
		
		dispatcher.forward(request, response);

	}
	/**
	 * 
	 * M�todo que accede al modelo de la clase Pel�cula (controladorPelicula) para pedirle la lista de peliculas
	 * y devolverlas a la vista prueba.jsp
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void obtenerlista(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		list = cont_pelicula.getLista();
		
		request.setAttribute("LISTAPELI", list);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		
		dispatcher.forward(request, response);		
	}
	/**
	 * M�todo que se encarga de cargar el driver de la BBDD y realizar la conexi�n, devolviendo
	 * un objeto de esa conexi�n
	 * 
	 * @return objeto de la Interfaz java.sql.Connection
	 */

	private Connection getConecction() {
		
		Connection c = null;
		
		try {
			
		Class.forName("org.hsqldb.jdbcDriver");
		c = DriverManager.getConnection("jdbc:hsqldb:mem:cineDB", "sa", "1234");		
		
		System.out.println("En linea");
		
		} catch (Exception e) {
	
			System.out.println("Error" + e.getMessage());
		}
		
		return c;
	}
	

	
	


}
>>>>>>> 04da192add67c894b0c328c7c56c8ac53fd881e8
